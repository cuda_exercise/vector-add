#include <cuda_runtime.h>

void vectorAdd(const float* a, 
               const float* b,
               const int n,
               float* c);