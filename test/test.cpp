#include <gtest/gtest.h>
#include <stdlib.h>

#include "vector_add.h"

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(AddVectorTest, Test1) {
    int n = 1 << 20;
    size_t size = sizeof(float) * n;
    float* a = (float*)malloc(size);
    float* b = (float*)malloc(size);
    float* c = (float*)malloc(size);
    float* c_ref = (float*)malloc(size);

    for (int i = 0; i < n; i++) {
        a[i] = rand() / (float)RAND_MAX;
        b[i] = rand() / (float)RAND_MAX;
    }

    vectorAdd(a, b, n, c);

    for (int i = 0; i < n; i++) {
        c_ref[i] = a[i] + b[i];
        EXPECT_EQ(c[i], c_ref[i]);
    }

    free(a);
    free(b);
    free(c);
    free(c_ref);
}